const path                    = require('path');
const webpack                 = require('webpack');
const ExtractTextPlugin       = require("extract-text-webpack-plugin");
const UglifyJSPlugin          = require('uglifyjs-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const BrowserSyncPlugin       = require('browser-sync-webpack-plugin');
const CleanWebpackPlugin      = require('clean-webpack-plugin');
const CopyWebpackPlugin       = require('copy-webpack-plugin');


const config = {
	entry: {
		app: './src/js/app.js',
		//vendor: './src/js/vendor/vendor.js'
	},
  output: {
      path: path.resolve(__dirname, 'dist'),
      filename: 'js/bundle.min.js',
      // publicPath: '/dist'
  },
	module: {
		rules: [
			{
				test: /\.scss$/,
				use: ExtractTextPlugin.extract({
					fallback: 'style-loader',
				  	use: ['css-loader', 'sass-loader', 'postcss-loader']
				}),
			},
			{
				test: /\.js$/,
				exclude: /(node_modules|bower_components)/,
				loader: 'babel-loader',
				query: {
					presets: ['es2015']
				}
			}
		]
	},
	plugins: [
    new CleanWebpackPlugin(['dist']),
		new ExtractTextPlugin('/css/app.css'),
		new BrowserSyncPlugin({
        // WATCH ANY FILES
        files: [
            '**/*.*'
        ],
        // STATIC
		    // browse to http://localhost:3000/ during development,
		    // ./dist directory is being served
		    //host: 'localhost',
		    //port: 3000,
		    //server: { baseDir: ['public'] },
        // DEVELOPMENT SERVER
        host: 'localhost',
        port: 3000,
        proxy: 'http://wpthemes.dev',
		}),
    new CopyWebpackPlugin([
            {from:'src/fonts',to:'fonts'},
            {from:'src/images',to:'images'}
    ]),
	],
};

//If true JS and CSS files will be minified
if (process.env.NODE_ENV === 'production') {
	config.plugins.push(
		new UglifyJSPlugin(),
		new OptimizeCssAssetsPlugin()
	);
}

module.exports = config;
