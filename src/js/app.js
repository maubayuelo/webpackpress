// css
import '../scss/app.scss';

// JavaScript

// PLUGINS
// load jquery
import $ from 'jquery';
import jQuery from 'jquery';
window.$ = $;
window.jQuery = jQuery;
// load slick (note: It needs jquery)
require('./vendor/slick/component.js');
// load fancybox (note: It needs jquery)
require('./vendor/fancybox/component.js');
