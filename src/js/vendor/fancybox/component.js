console.log('fancybox.js loaded');

require('./fancybox.js');

if ($("[data-fancybox]").length) // use this if you are using id to check
{
  $("[data-fancybox]").fancybox({
    buttons: [
      //'slideShow',
      //'fullScreen',
      //'thumbs',
      'close'
    ],
  });
}
