console.log('slick.js loaded');

require('./slick.js');

$(document).ready(function() {
  $(".slider").slick({
    dots: true,
    infinite: true,
  });
});
